import {SET_PROFILE} from '../constants/actionTypes'

const initialState = {
    username: "",
    image: "",
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case SET_PROFILE :
            return {
                ...state,
                username: action.payload.userName,
                image: action.payload.imageProfile,
            };
        default:
            return state
    }
}
