import Profile from "../containers/Profile";
import Scholar from "../containers/Scholar";
import Service from "../containers/Service";

export default [
    { path: '/profile',component: Profile },
    { path: '/scholar',component: Scholar },
    { path: '/service',component: Service },
    { path: '/',redirectPath: 'scholar'},
]