import Course from "../views/Service/Course";
import Grammar from "../views/Service/Grammar";
import Consult from "../views/Service/Consult";
export default [
    { path: '/service/course',component: Course},
    { path: '/service/grammar',component: Grammar},
    { path: '/service/consult',component: Consult},
]