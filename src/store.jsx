import { applyMiddleware,compose, createStore } from "redux";
import thunk from "redux-thunk";
import reducer from './reducers/index'
import promise from "redux-promise-middleware";

// const middleware = applyMiddleware(promise(), thunk);
const middleware = [promise(), thunk];
const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
        }) : compose;

const enhancer = composeEnhancers(
    applyMiddleware(...middleware),
    // other store enhancers if any
);


export default createStore(reducer,enhancer)
