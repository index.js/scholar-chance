import React, {Component} from 'react';
import Filter from "../../components/Scholar/Filter";
import ScholarItem from "../../components/Scholar/ScholarItem";
import Detail from '../../components/Scholar/Detail'
import Compare from '../../components/Scholar/Compare';

import sample from '../../static/data'

class Search extends Component {
  constructor(props){
    super(props);
  }
  
  state = {
    openModal: false,
    country1: true,
    country2: true,
    major1: '',
    major2: '',
    openCompareModal: false,
  }

  toggleModal = () => {
    this.setState({
      openModal: !this.state.openModal
    })
  }

  toggle = (name) => {
    this.setState({
      [name]: !this.state[name]
    })
  }

  render() {
    const scholarship = sample.scholarship
    return (
        <div>
          <div className="container-fluid">
            <div className={"row"}>
              <div className="col-lg-2 col-md-4"
                   style={{position: "fixed", height: "calc(100vh - 40px)", overflow: "auto", background: "#ffffff", zIndex: 9}}>
                <Filter
                  country1={this.state.country1}
                  country2={this.state.country2}
                  toggle={this.toggle}
                  style={{position: "fixed", height: "calc(100vh - 40px)", overflow: "auto", background: "#ffffff"}} />
              </div>
              <div className="col-lg-10 col-md-8 p-15 offset-lg-2 offset-md-4">
                <div className="text-right">
                  <h3>ผลการค้นหา</h3>
                  <h4>{scholarship.length} Searches</h4>
                </div>
                {scholarship.filter(item => {
                  if (this.state.country1 && item.country === 'USA') {
                    return true
                  }
                  if (this.state.country2 && item.country === 'ENG') {
                    return true
                  }
                }).map((item) => {
                  return <ScholarItem item={item} openModal={this.toggleModal} key={item.id}/>
                })}
              </div>
              {
                this.state.openModal && (
                    <div className="col-12">
                      <Detail closeModal={this.toggleModal}/>
                    </div>
                )
              }
            </div>
          </div>
          <Compare/>
        </div>
    )
  }
}

export default Search;