import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import videocall from '../../static/img/videocall.jpg'
import Footer from "../../components/Footer";

import Career1 from '../../static/img/career2.png'
import Career2 from '../../static/img/career1.png'
import Career3 from '../../static/img/career3.png'

class Step1 extends Component {
  render() {
    const nextStep = this.props.nextStep
    console.log(nextStep)
    return (
      <div className="row" style={{ minHeight: '70vh' }}>
        <div className="col-12">
          <h1>คุณอยากขอคำแนะนำจากใคร ?</h1>
        </div>
        <div className="col-md-4 col-6 my-2 text-center ">
          <div className="card career"  onClick={nextStep}>
            <div className="card-body">
              <img className="img-thumbnail no-border" src={Career1} alt="" />
              <div className="mt-2">
                <h3>จิรสิน สิงห์คำป้อง</h3>
                <span>26, ทุนม. ฮูสตัน USA</span>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-4 col-6 my-2 text-center " >
          <div className="card career" onClick={nextStep}>
            <div className="card-body">
              <img className="img-thumbnail no-border" src={Career2} alt="" />
              <div className="mt-2">
                <h3>คชาทัช นพวาร</h3>
                <span>29, ทุนม. แคมบริดจ์ UK</span>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-4 col-6 my-2 text-center " onClick={nextStep}>
          <div className="card career" onClick={nextStep}>
            <div className="card-body">
              <img className="img-thumbnail no-border" src={Career3} alt="" />
              <div className="mt-2">
                <h3>กันยา ขันอาษา</h3>
                <span>32, ทุนม. ฮูสตัน USA</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

class Consult extends Component {
  state = {
    showStep2: false
  }

  toggleStep = () => {
    this.setState({
      showStep2: !this.state.showStep2
    })
  }

  render() {
    return (
      <>
        {
          this.state.showStep2 ? (
            <div className="row justify-content-center">
              <div className="col px-0 d-flex justify-content-center">
                <div className="mx-auto d-inline-block position-relative">
                  <button onClick={this.toggleStep} className="no-button">
                    <img src={videocall} className="img-fluid" />
                  </button>
                </div>
              </div>
            </div>
          ) : (
            <Step1 nextStep={this.toggleStep} />
          )
        }
      </>
    )
  }
}

export default Consult;