import React, { Component } from 'react';
import img from '../static/img/109315-ONO2FQ-964-03.png';

class Footer extends Component {
  render() {
    return (
        <div className={"footer"}>
            <div className="p-15 container" style={{width: "100%",position: "relative"}}>
              <div style={{textAlign: "right", position: "absolute", right: 0,bottom: -50, zIndex: 0}} className={"pull-right"}><img height={400} src={img} alt=""/></div>
              <h1>Contact Us</h1>
              Name:<br/>
              <input type="text" className="input-line"/>
              Email:<br/>
              <input type="text" className="input-line"/>
              Your Message:<br/>
              <input type="text" className="input-line"/>
              <button className="btn btn-warning button-radius">Submit</button>
            </div>
          <div className="footer-tab">
            &#9400; copyright by YWC16
          </div>
        </div>
    );
  }
}

export default Footer