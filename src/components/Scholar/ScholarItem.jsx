import React, {Component} from 'react'
import {NavLink} from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardText
} from 'reactstrap'

class ScholarItem extends Component {

  openModal = (event) => {
    event.preventDefault()
    this.props.openModal()
  }

  render() {
    // const thumbnail = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAilBMVEX////0dzX0cy33nHT/+vj0byL0dTH0cCbzbRz0cyz2kWL5wan7z7383M/+8On0cSj2l2z1hEz3on75tJf96uP5uJ3zahT95NzzaQ7+9vL84db0ezzzbh370sH2jl35vaT4qIX1gkn1fkH6xrH4ro71iVX4sJH2lWj3oHr6ybbzZAD71sj70b/zXwCbyQB+AAAMy0lEQVR4nO2dfX+yLBTH1QRxPdhWMSvXw2pbrd3v/+3dqSgHRaWrUurD7z9SjK8oh3N40LKMjIwu027r23u361LcUWOCbRz4o67LcTf1iR3L+eq6IHfTJ04IbTrtuiR3kotSQBt9dF2UeylghGTYdUnupZ7P6vBpW9MRTV5E2u+6IPfTu0089MyAZw37g3nXZTAyMnpyTfc/h6ft0MRah4Hjk1XXxbifTmHSpfHeui7I3fTFfIvwaU1i7lucui7JvfT8hCvn2Z/SYdrSoE3XBbmfdmdrEdBx18W4p0az3uZpHXwjI6MWNd+tgfvgDtZHYP4+1jsQ0z+t+yA4vOj3F/cv3vWaURSgIOu1HMk5RXYsNXTOKfrNUqNP4nm0x1Luip5TK/0DjWsaG3Ucvicp5k2EgyQ1DZOuN5klKddLujiIIW6TeHGw7aTUF8hNB19sJ3UCl6k3gf0kNWa9Npo8thOPpZKbMWAZyaCroitqyApqR/HjNqI2HIvxhHGLV+ZLees49cbi4b7uzuMp8x9IXE/TjJfE9cRHZpJ62jLCIHlos/p1dO/TjdIXz8Z2kszqMEwakHx0LXkue4wpSnhnAeTVWT/p00bS0P0+rTZvk6SOKTAbIV2kNwPjJJU90Pp7Vq6NMMYke9a+yDmFXlnqQB2MPcwgZuE5FaQVall/vz7GfvjXdoH/QbOtvTrmqf6X/brOU4OVvZ3kJm/4s/zc5HU27S2XvWcdHTYyMmpT80V1W+K+L/Tvdzdo/hMSuqwI1EwiQsik3QLdWi72E79DGhA+JD08cmi7UDfVJoC9GFFD1qehDx2K81m/lEhc+Q3zLYJN68W6nWpnfT2Mb1ErO/MtJHW4z3yLffvlup0maSXiT8kx5mnY4UMEoyq1jb18h8VwCtrHU8IwfegqPGsTUbqqsPk7m1J7Jz/2SBrVeLij550ebWRk1KLWyzB6y9ua4zak47xlHa7CcJX3St/HNNzm4Z75IQqXPNyjr1bR2eYFiDWZm9gCOpmn0Y8HNXDIJhCfQie2jps0NY3OvVZMXlov8KVap30aJw0unlJvAqfz2qdZnyapUxelHTya4rNQMtG+FpfCipKsr42SZ/E765cmPvARwX74gnlWeNld2ZVU8C0+a8YtJow37cIOsoy0u8KrSVxRks3j85JXL/MP/U2c6rNhKpwMAOTDPVFnRVfUwWdvXpJibyV78z6y8cOkfrO3EiVvnsue0kD3kTdrHiXVFjIjsEyAySZNvSTV5rH2cpMA++zF26UDy0j7URtr+klQlI/qz1cUoTAb1bd64TmVB6K+zym6ypB25zPp50N0zBcDGGh7H3yAahkNBoBh/jEAbpZ7Gjy2a2xkZKSH5pOXl4n+9uDftSCB4wSycPiTyE17A5g8/ABbldasJ+pd7yG5QJee785Lv6hdpOJyIHM2f8jp1WRW0zgkTFTB3XJXlHD9l7gB61/wE/m9bHmIB/NSvrvCgRH61w8gZjfr/My/Np48X/p2LozSZiBzbdhNv2gp2kck5OVxiR3zNND1Ue9LCKcB5sXxl6zXKBJetgfECsOsgNCyk3I59oU4El1AeCKgPMFX9tIUCFOfVU1TKmSFhKMlCQKyvIH/oE64g8VBfOSyQJhOJVXTxhdyOkL0bLDf32T6rDLhBAJmfmqsIqGnvIVANoNYTngrqRK+wdJQOOuzSIiV351izk4JXxAoSXiEh4rltImqwVhiMWOHhO424OXAhd1WSoSqBuNEihk7IxzZDi8GRgVzUCJUNRg/TiGfSHiazW6yuFSBcIHA0+Qsix5NmVDNYIxoMR8knG9ja7G9gfvUTPhBoRl8LdmCMqGawdgHxWyQ8DO1+LJ5GheqkbAvmMEfyQllQiWPAJWyAcIj685Fx5orqKmJcA+bAyKLMksIVQzGrpbwcLsVJQ2EPVgOKp0MKSFUMRhbXMoFCG/oPdUSuiv4rlB5P19G2GwwFkVTIRLOMg/4+hUldYTzT+gs0Yp6kRE2G4xe0VSIhPO0ecP0+sa0hnDqC2ZQOjnLqiBsMhjzUJIJtqWnMIjXl9zAIlYTDqGz5NiVfoyU0Kb1BmNWMhUFQmu02W43txh8qSQ8wrssMYO5ICGv9AaD4ed3D/P72G6vbQbNoFf314Bw+cavVhv1GfAmennI87RKuBHMYG04CBD6U56N1G2P9JpXXDDjbnCbhD/NZjAXIET5aPz5cjUG450/IKH71gGh+yqYwQafXSAEj1+NwQBQPasDQtFZok2bcQmEoAmpNhhz8CwvOiB894CVwFHj2IhIuOapSg+DnxNPH2md8ETVzGAukdDlMd5Kg2Hn/xAvcm6bUHCWnE8FR08kBBHCKg+DRy+ShfktE35DQOk6gZIKhCDKG8lf4Zf8L4O4ltslPAjjCGylfIMKhABA7mGAW5Cs0m+VcFXsYoYKMaUiIQihSQ3GJjdFaWvbIuH2Swyyx7/ZzS9ikTCfZSg3GG7RYrZHCPvAufzm2GeJEMQnJIPU/Ch7iFsklAp9N12hRAhiTBKDwWuYhTq6IHRge0ObtjUoE37nb1rZYAy5qWDeRweE5DAVhmEaWpsy4YhnL60tHOf/h1h/t33C2JUYALPY1NqUCWHzVXiPITy7bNuErKe9B+6TX//HEsIFv0FUDO9M8gc4XzvZMqHjsxJ9gd/rWxsJIXATCwaDn0uzHm+7hDweM/eB7ahtbWSEf8DowWf8yE1FPkzeKiECEeYFjESRmtZGRmhx2yoYDB694IOQbRIKg9fWGjSodQMRUkLgAoKsPNCN+TZlLRIGhX72GMQyalobKSFwE4HB4GE1xMeUuojTZILj7NWtjZQQuon5XC4Q6Pb4mV0Sioa/Kl4jJwQ+Ut5l4IFuuIVXl4SC4a9sbeSEwE3MDQaPXhAw4NIpobUBDmNVFLuCELiJjIdP0xNmHHZLaL2CptaXDHFblYTAiWAGg3cDhI5Ox4RzOB0RNY0BC4TcEUz3Z+FvJha+0tUxYbbpNrv3stamitDiBiMJSfHWVYxQdU0oGH6bSMKnlYS8lx17GNxCFroPnRMKhl/W2lQSjoSQFD+t4Ph3TygYfklrU0kIrn02GPwqheCNBoSC4S+3NtWEwE0M+KBUMQCnAaFo+EuTB6oJgX2weaCyGBXRgVAw/KXWpobwrzzvqYyhBaHg8RdHM2oILUkItjRdSg9CwfAHYmtTR7iGlS+9P7oQioYfCZ5yHWFxrrpsiYgmhKLhF1qbOsLiegPZKboQCobfjkBrU0tYWDNiB2VHWhtCwfDDt6mWELiJae2Xu336EAqGP5CumZEQihPyZdNF9SEUDX+Utzb1hMBNtOWbQGpEKBr+vLVpIISznaV/oROhYPhtVF6dJyOEM9al8wK0IhQMP3bS8EsTIXATZXux3p1wjAImT2FuyTAMuFA67Lam+S+hLM8oz0Okk7YPJL/gXT6FuJvkUpk0/vHNz598J2OcQ/6TPGrczw7vpZO2j/yCz/1pYCOjSv3DZgHu/XQPwjFFTKHiBujTX3Qn3WefQG4PVT/pMpWs57mN7rO1tSE0hIbQEOpO6EgEPV4sOV59OJAPwXZJiMcSgR1Z8I/kODj8JR75ucse81cREtnxBR8ZJTJngnvEss8+3F7XEcq6WUNAKBlQBfO8lZYCXC1DWJYhNISG8NYyhGUZQkNoCG8tQ1iWITSEhvDWMoRlGUJDaAhvLUNYliE0hIbw1jKEZRlCjQkVZ5u8Py6h4ujdonaDHZ0JVTclPj0Y4eV7ax4Boey4boRgZ4Bt89mxwPaxD0HI5yjL5zCXxWvd9mXHdSMEs7TrNjAB4ssLxDXZmXQjBO0GUtrhHWzN6Uj3ptWNENhvtf3PP3gJ5X0E3QjBvkZqX58F270g6W7b2hGCpVcq/whXFEqny+hHuOaNqcKaEoXTtSOEKwRJ41+C8lV11bUjhIvLsN80l7wH1oRWbManH2EfVEvTVxv6cHcsqTXUkdCFa+68WotxFPYcqph3px9h/rWM9OX6qv6ihPBhsspurIaEljDp1fEqPjm4eBV2SKBD+WlaEp7EBdfInpVK5g5WVNgfIah8nHUktN7ErQ9wQD43x+E0aVjd0fvfZBUhcf8HjCtbXS0JhQ2vUgLfiwilESKUEBSUtreo2bZVT0J32bCdcBGw5vsvehKK31BtEg7rJmhrSmi5K8leMnI5Xu33EnQltKzvULKft0RRjcWMpS+htdhGzYxB1LQcWWPCc69sWc94NiP7xnWeWhNa1t+KehWQZzx7pvCRMM0JLWvUX0XE8wVM7ASILL/VPlavPWFciEX/7cvPvzFOo+14dlL+QNiQf4n8PxkhP/x7/ecbr9No+n7WdHThCuv5O5csKzh8g6+MGhk9nP4H0AbfxMh3060AAAAASUVORK5CYII='
    const { thumbnail } = this.props.item || ''
    return (

        <Card className="my-3">
          <CardBody>
            <Row>
              <Col xs={3} md={2}>
                <CardImg className="img-thumbnail" src={thumbnail}/>
              </Col>
              <Col xs={9} md={10}>
                <CardTitle>
                  {this.props.item.name}
                </CardTitle>
                <CardText>
                  <p>จำนวนทุน: {this.props.item.max}</p>
                  <p>กำหนดยื่นสมัคร: {this.props.item.startdate}</p>
                  <p>มูลค่าทุนการศึกษา:{this.props.item.amount}</p>
                  <div className="text-right">
                    <NavLink to="/scholar/1" onClick={this.openModal} className="scholar-item-link">
                      <i className="fa fa-heart text-danger"/>
                      <button className={"btn btn-warning btn-sm"} style={{marginLeft: "20px", borderRadius: "20px"}}>อ่านเพิ่มเติม</button>
                    </NavLink>
                  </div>
                </CardText>
              </Col>
            </Row>
          </CardBody>
        </Card>

    )
  }
}

export default ScholarItem
