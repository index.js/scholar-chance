import React, {Component} from "react";
import {removeItem} from "../../actions/itemActions";
import {connect} from 'react-redux';
import CompareModal from "../../components/Scholar/CompareModal";

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state={
      openCompareModal: false
    }

  }

  toggleCompareModal = () => {
    this.setState({
      openCompareModal: !this.state.openCompareModal
    })
  }


  render() {
    let ele = [];
    for (let i = 0; i < this.props.item; i++) {
      ele.push(
          <div className="compare-box" key={i}>
            <div className="pull-right text-danger">
              <div onClick={this.props.removeItem} style={{cursor: "pointer"}}>
                <i className="fa fa-times-circle"/>
              </div>
            </div>
            <img height={50} src={"https://secure.skypeassets.com/content/dam/scom/skype-features/skype-video-call.jpg"}
                 alt=""/>
            <div className={"compare-title"}>IELTS Extension</div>
          </div>
      );
    }
    return this.props.item <= 0 ? null : (
        <div className={"compare-bar box-shadow"}>
          <div className="container-fluid">
            {ele}
          </div>
          <div className="compare-action">
            <button className="btn btn-info" style={{width: "100%"}}>เปรียบเทียบ</button>
          </div>
          {
            this.state.openCompareModal && (
                <div className="col-12">
                  <CompareModal closeModal={this.toggleCompareModal}/>
                </div>
            )
          }
        </div>
    )
  }
}

export default connect(store => {
  return {item: store.item.count}
}, {removeItem})(Filter);